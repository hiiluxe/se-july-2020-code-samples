class Clock extends React.Component {
  state = {
    date: new Date(),
    format: "precise",
  };

  componentDidMount() {
    // this is a great place in class components to make API requests
    // also to set up timers
    setInterval(this.tick, 1000);
  }

  tick = () => {
    this.setState({
      date: new Date(),
    });
  };

  changeFormat = () => {
    const format = dateFormats[randomNumber(0, dateFormats.length - 1)];
    this.setState({
      format,
    });
  };

  render() {
    return (
      <div id="clock" onClick={this.changeFormat}>
        <Time {...this.state} />
      </div>
    );
  }
}
