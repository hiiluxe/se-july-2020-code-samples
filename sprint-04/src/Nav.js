import { Link } from "react-router-dom";
import { Nav as BootstrapNav } from "react-bootstrap";

const NavigationLink = ({ to, children }) => (
  <BootstrapNav.Item>
    <BootstrapNav.Link as={Link} to={to}>
      {children}
    </BootstrapNav.Link>
  </BootstrapNav.Item>
);

const Nav = () => {
  return (
    <BootstrapNav>
      <NavigationLink to="/">Connect Class</NavigationLink>

      <NavigationLink to="/use-redux">Use Redux</NavigationLink>
      <NavigationLink to="/swapi">Galaxies Bruh...</NavigationLink>
    </BootstrapNav>
  );
};

export default Nav;
