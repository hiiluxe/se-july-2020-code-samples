import React from "react";

export class ClassesExample extends React.Component {
  state = {
    num: 0,
  };
  componentDidMount() {
    console.log(`Classes ${this.props.name} mounted!`);
  }

  componentWillUnmount() {
    console.log(`Classes ${this.props.name} unmounted!`);
  }

  reRender = () => {
    this.setState({ num: Math.random() });
  };

  render() {
    console.log(`Classes ${this.props.name} rendered!`);
    return (
      <>
        <h1>Classes my name is {this.props.name}</h1>
        <button onClick={this.reRender}>Render</button>
      </>
    );
  }
}
