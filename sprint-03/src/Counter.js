import { useRef, useEffect } from "react";
import useTickTock from "./hooks/useTickTock";
import useMount from "./hooks/useMount";
import useTitle from "./hooks/useTitle";
export const Counter = () => {
  const { count, increment, decrement, reset } = useTickTock();
  const inputRef = useRef();
  const incrementRef = useRef();

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  useTitle(`I am ${count} years old`);

  useMount(() => console.log("Counter mounted bruh.."));
  return (
    <>
      <p>{count}</p>
      <input ref={inputRef} value={count} onChange={(e) => {}} />
      <button ref={incrementRef} onClick={increment}>
        +
      </button>
      <button onClick={decrement}>-</button>
      <button onClick={reset}>Reset!!!</button>
      <button onClick={() => incrementRef.current.click()}>This is a bad idea, dont do this please.....</button>
    </>
  );
};
