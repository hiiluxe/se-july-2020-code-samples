import { Link, NavLink } from "react-router-dom";
import { Nav as BootstrapNav } from "react-bootstrap";

const NavigationLink = ({ to, children }) => (
  <BootstrapNav.Item>
    <BootstrapNav.Link as={Link} to={to}>
      {children}
    </BootstrapNav.Link>
  </BootstrapNav.Item>
);

const Nav = () => {
  return (
    <BootstrapNav>
      <NavigationLink to="/">Home</NavigationLink>

      <NavigationLink to="/classes-example">Classes Example</NavigationLink>

      <NavigationLink to="/hooks-example">Hooks Example</NavigationLink>

      <NavigationLink to="/counter">Counter Example</NavigationLink>

      <NavigationLink to="/species">Species</NavigationLink>

      <NavigationLink to="/starships">Starships</NavigationLink>

      <NavigationLink to="/pokemon">Pokemon</NavigationLink>

      <NavigationLink to="/example">Example</NavigationLink>
    </BootstrapNav>
  );
};

export default Nav;
